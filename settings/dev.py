from .base import *
from decouple import config
DEBUG = True
SECRET_KEY=config("SECRET_KEY")

ALLOWED_HOSTS = ['*', ]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('dev_db_name'),
        'USER': config('dev_db_username'),
        'HOST': config("dev_db_host"),
        'PASSWORD': config("dev_db_password"),
        'PORT':5432,

    },
    'slave1': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('dev_slave1_name'),
        'USER': config('dev_db_username'),
        'HOST': config("dev_db_host"),
        'PORT': 5432

    },
    'slave2': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('dev_slave1_name'),
        'USER': config('dev_db_username'),
        'HOST': config("dev_db_host"),
        'PORT': 5432

    },
    'slave3': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('dev_slave3_name'),
        'USER': config('dev_db_username'),
        'HOST': config("dev_db_host"),
        'PORT': 5432

    },

}

# DATABASE_ROUTERS = ['rpcserver.routers.ReplRouter',state_routers.MasterSlaveReplica,transaction_routers.logrouter]
REPLICATED_DATABASE_DOWNTIME = 20

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = config('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = True

ENVIRONMENT_NAME = "rkoWallet"
#ENVIRONMENT_COLOR = "#FF2222"

ENVIRONMENT_COLOR="#FF2222"

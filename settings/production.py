from .base import *
from decouple import config

DEBUG = False
SECRET_KEY=config("SECRET_KEY")

ALLOWED_HOSTS = ['*', ]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('prod_db_name'),
        'USER': config('prod_db_username'),
        'PASSWORD': config('prod_db_password'),
        'HOST': config("prod_db_host"),
    },
    'slave1': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('prod_db_name'),
        'USER': config('prod_db_username'),
        'PASSWORD': config('prod_db_password'),
        'HOST': config("prod_db_host"),
    },
    'slave2': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('prod_db_name'),
        'USER': config('prod_db_username'),
        'PASSWORD': config('prod_db_password'),
        'HOST': config("prod_db_host"),
    },

}
REPLICATED_DATABASE_SLAVES = ['slave1', 'slave2']
REPLICATED_DATABASE_DOWNTIME = 20

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = config('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = True

ENVIRONMENT_NAME = "rkoWallet"
#ENVIRONMENT_COLOR = "#FF2222"

ENVIRONMENT_COLOR="#FF2222"
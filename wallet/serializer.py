from  rest_framework import serializers
class DepositSerializer(serializers.Serializer):
	amount=serializers.IntegerField(required=True)
	mobile_number=serializers.CharField(required=True)

class WithdrawSerializer(serializers.Serializer):
	amount = serializers.IntegerField(required=True)
	mobile_number = serializers.CharField(required=True)
	pin = serializers.IntegerField(required=True)

from django.db import models
import datetime
# Create your models here.
from users.models import User
class Deposit(models.Model):
	amount=models.IntegerField(default=0)
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	date=models.DateTimeField(default=datetime.datetime.now)
	class Meta:
		ordering=['-date']
		verbose_name='Deposit'
		verbose_name_plural='Deposits'
	def __str__(self):
		return self.user.mobile_number
class Withdrawals(models.Model):
	amount=models.IntegerField(blank=False)
	mobile_number=models.CharField(max_length=100)
	date=models.DateTimeField(default=datetime.datetime.now)
	class Meta:
		ordering=['-date']
		verbose_name='Withdrawal'
		verbose_name_plural='Withdrawals'
	def __str__(self):
		return  self.mobile_number
class UserWallet(models.Model):
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	balance=models.IntegerField(default=0)
	last_credit=models.ForeignKey(Deposit,on_delete=models.CASCADE)
	last_debit=models.ForeignKey(Withdrawals,on_delete=models.CASCADE)
	last_credit_date=models.DateTimeField(default=datetime.datetime.now)
	last_debit_date=models.DateTimeField(default=datetime.datetime.now)
	last_activity=models.CharField(max_length=100)
	last_activity_date=models.DateTimeField(default=datetime.datetime.now)
	def __str__(self):
		return self.user.mobile_number
	class Meta:
		ordering=['-last_activity_date']
		verbose_name='Wallet'
		verbose_name_plural='Wallets'
	def mark_credits(self,amount,deposit):
		self.balance+=amount
		self.last_credit=deposit
		self.last_credit_date=datetime.datetime.now()
		self.last_activity='Credit'
	def markdebits(self,amount,withdrawal):
		self.balance-=amount
		self.last_debit=withdrawal
		self.last_debit_date=datetime.datetime.now()
		self.last_activity='Debit'





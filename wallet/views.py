import io

import datetime
import random

import django
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views import View
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.authentication import  BasicAuthentication

from wallet.serializer import DepositSerializer
from processes.wallet_backend.deposit_backend import Backend as  DepositBackend
from processes.wallet_backend.withdraw_backend import Backend as  WithdrawBackend
class UserDeposit(APIView):
	authentication_classes = (BasicAuthentication,)
	permission_classes = ((AllowAny,))
	get_serializer=lambda *args,**kwargs:DepositSerializer(*args,**kwargs)
	def post(self,request,*args,**kwargs):
		try:
			serializer=UserDeposit().get_serializer(
				data=request.data,
				context={"request":request}
			)
			serializer.is_valid(raise_exception=True)
			return JsonResponse(
				DepositBackend().deposit(
					mobile_number=serializer.validated_data['mobile_number'],
					amount=serializer.validated_data['amount']
				)
			)
		except Exception as e:
			raise e
class UserWothdraw(APIView):
	authentication_classes = (BasicAuthentication,)
	permission_classes = ((AllowAny,))
	get_serializer = lambda *args, **kwargs: DepositSerializer(*args, **kwargs)
	def post(self,request,*args,**kwargs):
		try:
			serializer=UserWothdraw().get_serializer(
				data=request.data,
				context={"request":request}
			)
			serializer.is_valid(raise_exception=True)
			return JsonResponse(
				WithdrawBackend().withdraw(
					mobile_number=serializer.validated_data['mobile_number'],
					amount=serializer.validated_data['amount'],
					pin=serializer.validated_data['pin']
				)
			)
		except Exception as e:
			raise e

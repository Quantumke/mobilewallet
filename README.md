![pyRKO](https://raw.githubusercontent.com/Quantumke/files/master/pyRKO.png)

rkoWallet
==========

This project demostrates how to create a mobile wallet that
1.allows users to deposit money to thier wallets via MPESA,
2. allows users to withdraw the same cash via MPEA
3. allows user to accrue interests
4. analizes  user activities and monetary activities to calculate
in respect to industries stardands loan limits
5. Grants and manages loans
6. Interwallet transfer
7. Wallet as a checkout 
8. Mini statements

Instructions
================
1. Clone repo
2. Install requirements.txt
3. once in root of the folder run: *touch .env*

copy 
```ini
SECRET_KEY=
dev_db_host=localhost
dev_db_username=
dev_db_name=
dev_db_password=
dev_slave1_name=
dev_slave3_name=
prod_db_host=
prod_db_username=
prod_db_password=
prod_db_name=
at_key=
at_username=
```
`at_key is Africa's Talking api key`

 `at_username is Africa's Talking Username`
 
 *Generate auth token from admin portal*
 
 ##### Register new useer
 
```
URL /user/register/
METHOD POST
headers={'Content-Type':'application/json',
'Authorization':'Token token'}
body={
	"mobile_number":"",
	"first_name":"",
	"last_name":"",
	"id_number":"",
	"password":""
}
RESPONSE: content-type:application/json

{
    'code':0,
    'status':True,
    'message':'success'
}
```

#### Deposit

Utilizes [READ THE DOCS](https://python-mpesawrapper.readthedocs.io/en/latest/)

Follow the Mpesa Express API to enable real deposits

This will credit a user wallet 

```
URL /wallet/deposit/
METHOD POST
headers={'Content-Type':'application/json',
'Authorization':'Token token'}

body={
    'mobile_number':'',
    'amount':''
}
RESPONSE: content-type:application/json

{
    'code':0,
    'status':True,
    'message':'success'
}

```

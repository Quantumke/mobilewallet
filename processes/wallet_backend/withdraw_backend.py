from processes.common import CommonHandler
from  users.models import User
from  wallet.models import UserWallet,Withdrawals
from  python_mpesawrapper import B2CHandler
class Backend:
	@staticmethod
	def withdraw(**kwargs):
		try:
			pin=kwargs.pop('pin')
			amount=kwargs.pop('amount')
			mobile_number=kwargs.pop('mobile_number')
			is_valid=User.objects.filter(pin=pin)
			if is_valid:
				wallet=UserWallet.objects.get(mobile_number=mobile_number)
				if int(amount)<= wallet.balance :
					B2CHandler().handle(amount=int(amount), mobile_number=mobile_number)
					#handle status in callback
					withdrawal=Withdrawals(amount=amount,mobile_number=mobile_number)
					wallet.markdebits(amount,withdrawal)
					text="You have succesfully transferd ksh {0} is usccesful your new balance is {1}".format(str(amount),str(wallet.balance))


				else:
					text='Wallet balance  too low'
			else:
				text='Invalid pin'
			CommonHandler().send_sms(text, mobile_number)
		except Exception as e:
			raise  e
		except UserWallet.DoesNotExist as DE:
			raise  DE

'''
1. Check pin
2.check user balance
3. send money
4. debit account
5. Report to wallet
'''
# from  python_mpesawrapper import MpesaExpressHandler
from processes.common import CommonHandler
from wallet.models import Deposit,UserWallet
from  users.models import User

class Backend:
	@staticmethod
	def deposit(**kwargs):
		try:
			mobile_number=kwargs.pop('mobile_number')
			amount=kwargs.pop('amount')
			#MpesaExpressHandler().handle(mobile_number=mobile_number, amount=amount)
			userobj=User.objects.get(mobile_number=mobile_number)
			saveDeposit=Deposit(amount=amount,user=userobj)
			saveDeposit.save()
			#use filter to avoid version conflicts
			walletExist=UserWallet.objects.filter(user=userobj)
			if walletExist:
				wallet=UserWallet.objects.get(user=userobj)
			else:
				wallet=UserWallet(user=userobj)
			wallet.mark_credits(amount,saveDeposit)
			deposit_sms='You have succsfully deposited {0} to your rkoWallet. Your balance is {1}'.format(str(amount),str(wallet.balance))
			CommonHandler().send_sms(deposit_sms, mobile_number)
		except Exception as e:
			raise e
		except User.DoesNotExist as DE:
			raise  DE
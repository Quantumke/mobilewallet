from random import randint
from  wallet.models import UserWallet
from processes.common import CommonHandler
from  users.models import User
class Backend:
	@staticmethod
	def register_user(**kwargs):
		response={}
		try:
			mobile_number = kwargs.pop('mobile_number')
			pin = randint(1000, 9999)
			user=User(
				mobile_number=mobile_number,
				first_name=kwargs.pop('first_name'),
				last_name=kwargs.pop('last_name'),
				id_number=kwargs.pop('id_number'),
			)
			user.set_password(kwargs.pop('password'))
			user.pin=pin
			user.save()
			pin_sms='Welcome to rkoWallet, your wallet pin is {0}'.format(pin)
			CommonHandler().send_sms(pin_sms, mobile_number)
			wallet=UserWallet(user=user)
			wallet.save()
			response['code']=0
			response['status']=True
			response['message']='User was saved succesfully'
			return response
		except Exception as e:
			response['code'] = 1
			response['status'] = False
			response['message'] = str(e)
			return response
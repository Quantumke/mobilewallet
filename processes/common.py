import africastalking
from  decouple import config
class CommonHandler:
	@staticmethod
	def send_sms(text,mobile_number):
		try:
			if mobile_number.startswith("254"):
				mobile_number = "+"+mobile_number
			elif mobile_number.startswith("0"):
				mobile_number = "+254"+mobile_number[1:]
			africastalking.initialize(config('at_username'), config('at_key'))
			sms = africastalking.SMS
			sms.send(text, [mobile_number])
		except Exception as e:
			raise  e
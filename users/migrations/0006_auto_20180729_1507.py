# Generated by Django 2.0.5 on 2018-07-29 15:07

from django.db import migrations, models
import hashid_field.field


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_user_points'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(default=True, help_text='is active.', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_staff',
            field=models.BooleanField(default=False, help_text='is staff.', verbose_name='staff status'),
        ),
        migrations.AlterField(
            model_name='user',
            name='pin',
            field=hashid_field.field.HashidField(alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', default=3394, min_length=7),
            preserve_default=False,
        ),
    ]

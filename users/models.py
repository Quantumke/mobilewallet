from random import randint
from django.db import models
from  django.contrib.auth.models import BaseUserManager,AbstractBaseUser,PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from  processes.common import CommonHandler
from hashid_field import HashidField
# Create your models here.

class WalletUserManager(BaseUserManager):
	def _create_user(self,mobile_number,password,**extra_fields):
		try:
			pin=randint(1000,9999)
			#send sms
			CommonHandler().send_sms(str(pin),mobile_number)
			if not  mobile_number:
				raise  ValueError('Mobile Number Missing')
			user=self.model(mobile_number=mobile_number,**extra_fields)
			user.set_password(password)
			user.pin=pin
			user.save()
			return user
		except Exception as e:
			raise e
	def create_superuser(self,mobile_number,password,**extra_fields):
		try:
			extra_fields.setdefault('is_staff', True)
			extra_fields.setdefault('is_superuser', True)
			extra_fields.setdefault('is_active', True)
			return  self._create_user(mobile_number,password,**extra_fields)
		except Exception as e:
			raise e

class User(AbstractBaseUser,PermissionsMixin):
	mobile_number=models.CharField(max_length=100,unique=True)
	first_name=models.CharField(max_length=100,blank=True,null=True)
	last_name=models.CharField(max_length=100,blank=True,null=True)
	id_number=models.CharField(max_length=100,blank=True,null=True)
	pin=HashidField(allow_int_lookup=True)
	points=models.IntegerField(default=0)
	is_staff = models.BooleanField(
		_('staff status'),
		default=False,
		help_text=_('is staff.'),
	)
	is_active = models.BooleanField(
		_('active'),
		default=True,
		help_text=_(
			'is active.'
		),
	)
	USERNAME_FIELD= 'mobile_number'
	objects=WalletUserManager()
	def __str__(self):
		return self.mobile_number

from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from  users.serializers import UserRegistrationSerializer
from processes.user_backend.backend import Backend
class RegisterUsers(APIView):
	authentication_classes = (BasicAuthentication,)
	permission_classes = ((AllowAny,))
	get_serializer=lambda *args,**kwargs:UserRegistrationSerializer(*args,**kwargs)
	def post(self,request,*args,**kwargs):
		try:
			serializer=RegisterUsers().get_serializer(
				data=request.data,
				context={'request':request}
			)
			serializer.is_valid(raise_exception=True)

			'''
			1. Send notification status
			2. Add new user with mobile number as username
			3. Create points metrics
			'''
			return  JsonResponse(
				Backend().register_user(
					mobile_number=serializer.validated_data['mobile_number'],
					first_name=serializer.validated_data['first_name'],
					last_name=serializer.validated_data['last_name'],
					id_number=serializer.validated_data['id_number'],
					password=serializer.validated_data['password']

				)
			)
		except Exception as e:
			raise  e

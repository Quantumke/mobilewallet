from rest_framework import  serializers
class UserRegistrationSerializer(serializers.Serializer):
	mobile_number=serializers.CharField(required=True)
	first_name=serializers.CharField(required=True)
	last_name=serializers.CharField(required=True)
	id_number=serializers.CharField(required=True)
	password=serializers.CharField(required=True)
	def update(self, instance, validated_data):
		pass